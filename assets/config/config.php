<?php

// MySQL Database server
define('CONFIG_HOSTNAME', '{{DB_HOST}}');

// User that accesses the database server, that should have all privileges on the database CONFIG_DATABASE
define('CONFIG_USERNAME', '{{DB_USER}}');

// User password
define('CONFIG_PASSWORD', '{{DB_PASS}}');

// Database for Team Password Manager. You must manually create it before installing Team Password Manager
define('CONFIG_DATABASE', '{{DB_NAME}}');
